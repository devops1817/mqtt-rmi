package rocha.igor.service;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import rocha.igor.reader.MqttServerReaderImpl;
import rocha.igor.reader.interfaces.MqttServerReader;

public class TempPublisherService {

    private static final String CLIENT_ID = "controller_2_temp_manager";
    private static final String TOPIC = "greenhouse_temp_manager";
    private static MqttServerReader reader =  new MqttServerReaderImpl();
    private static MqttClient client;

    public TempPublisherService() throws MqttException {
        client = new MqttClient(reader.getBrokerUrl(), CLIENT_ID, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        client.connect(options);
    }

    public void publishIncreaseTemp(double temp) throws MqttException {
        String message = "increase_temp: " + temp + "";
        client.publish(TOPIC, new MqttMessage(message.getBytes()));
    }
    public void publishLessTemp(double temp) throws MqttException {
        String message = "less_temp: " + temp + "";
        client.publish(TOPIC, new MqttMessage(message.getBytes()));
    }


}
