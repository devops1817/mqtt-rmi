package igor.rocha;

import java.util.Random;

public class RandomTemp {

    private static Random random  = new Random();

 
    public Double getRandomTemp(Double minimum, Double maximum){
        return random.nextDouble((maximum - minimum) + 1) + minimum;
    }
}
