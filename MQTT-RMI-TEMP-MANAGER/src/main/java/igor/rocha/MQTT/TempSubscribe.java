package igor.rocha.MQTT;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class TempSubscribe {

    private static final String BROKER_URL = "tcp://localhost:1883";
    private static final String CLIENT_ID = "greenhouse_manage_sub";
    private static final String TOPIC = "greenhouse_temp_manager";

    private static final TempPublisher publisher;

    static {
        try {
            publisher = new TempPublisher();
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
    }

    private MqttClient client;


    public TempSubscribe() throws MqttException {
        client = new MqttClient(BROKER_URL, CLIENT_ID, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        client.connect(options);
        client.subscribe(TOPIC, new IMqttMessageListener() {
            @Override
            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                publisher.publishTemp(mqttMessage);
            }
        });
    }



}
