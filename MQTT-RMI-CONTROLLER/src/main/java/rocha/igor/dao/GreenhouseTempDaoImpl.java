package rocha.igor.dao;

import rocha.igor.dao.interfaces.GreenhouseTempDao;
import rocha.igor.entities.GreenhouseTemp;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GreenhouseTempDaoImpl implements GreenhouseTempDao {
    private Connection conn;

    @Override
    public Integer insertGreenhouseTemp(String date, double temp) {

            String sql = "INSERT INTO greenhouse_temp VALUES (default, ? , ? , ? , ?)";

            try {
                startConnection();
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setDate(1, Date.valueOf(LocalDate.parse(date.substring(0, 10), DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
                ps.setString(2, date.substring(11));
                ps.setDouble(3, temp);
                ps.setString(4, "less");
                int result = ps.executeUpdate();
                return result;

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Connection database error.");
                return -1;

            } finally {
                try {
                    if(this.conn != null){
                        this.conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.err.println("Close connection error");
                }
            }
    }

    public List<GreenhouseTemp> searchByDate(Date date) {
        String sql = "SELECT * FROM greenhouse_temp WHERE date_temp = ?;";

        List<GreenhouseTemp> listTemps = new ArrayList<>();
        try {
            startConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setDate(1,date);
            ResultSet result = ps.executeQuery();

            while(result.next()){
                GreenhouseTemp greenhouseTemp = new GreenhouseTemp();
                greenhouseTemp.setId(result.getLong("id"));
                greenhouseTemp.setDateTemp(result.getDate("date_temp").toLocalDate());
                greenhouseTemp.setTimeTemp(result.getString("time_temp"));
                greenhouseTemp.setTemp(result.getDouble("temp"));
                listTemps.add(greenhouseTemp);

            }
            return listTemps.size() > 0 ? listTemps : null;

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;

        } finally {
            try {
                if(this.conn != null){
                    this.conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }
    }


    private void startConnection() throws SQLException {
        if(this.conn == null || this.conn.isClosed()){
            this.conn = ManageConnection.CreateDatabaseConnection();
        }
    }
}
