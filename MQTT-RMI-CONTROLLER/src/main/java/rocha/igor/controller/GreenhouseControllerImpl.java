package rocha.igor.controller;

import org.eclipse.paho.client.mqttv3.MqttException;
import rocha.igor.controller.interfaces.GreenhouseTempController;
import rocha.igor.dao.GreenhouseTempDaoImpl;
import rocha.igor.entities.GreenhouseTemp;
import rocha.igor.service.TempPublisherService;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Date;
import java.util.List;


public class GreenhouseControllerImpl extends UnicastRemoteObject implements Serializable, GreenhouseTempController {

    private static Double lastTemp;
    private static String dateLastTemp;

    private static Double maxTemp = 28.3;

    private static Double minimumTemp = 26.5;

    private static GreenhouseTempDaoImpl dao = new GreenhouseTempDaoImpl();

    private static TempPublisherService publisherService;

    static {
        try {
            publisherService = new TempPublisherService();
        } catch (MqttException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    public GreenhouseControllerImpl() throws RemoteException {
    }

    @Override
    public String getLastTemp() throws RemoteException {
        return String.valueOf(lastTemp);
    }

    @Override
    public String getDateLastTemp() throws RemoteException {
        return dateLastTemp;
    }

    @Override
    public void setMaxTemp(double temp) {
        maxTemp = temp;
    }

    @Override
    public void setMinTemp(double temp) {
        minimumTemp = temp;
    }

    @Override
    public void lowerTemp(double temp) throws MqttException{
        publisherService.publishLessTemp(temp);
    }

    @Override
    public void increaseTemp(double temp) throws MqttException {
        publisherService.publishIncreaseTemp(temp);
    }

    @Override
    public List<GreenhouseTemp> searchByDate(Date date) throws RemoteException {
        return dao.searchByDate(date);
    }


    public static void setTemp(String date, String temp) {
        dateLastTemp = date;
        lastTemp = Double.valueOf(temp);
        if(lastTemp > maxTemp ) dao.insertGreenhouseTemp(date, lastTemp);
        else if (lastTemp < minimumTemp) dao.insertGreenhouseTemp(date, lastTemp);

    }



}
