# Relatório Trabalho Prático De Sistemas Distribuídos


## Sistema De Controle Da Temperatura De Uma Estufa

Este sistema foi feito com o objetivo de monitorar e controlar a temperatura de uma Estufa.

Abaixo temos um **overview** do projeto.

![](https://i.imgur.com/2ASEQav.png)

Onde temos: 

- Clientes;
- Controladores;
- Bancos de dados;
- Servidor MQTT;
- Servidores RMI;
- O Sensor;
- O Atuador.


### Clientes

No sistema do cliente, podemos acessar os controladores por meio de **Servidores RMI**, estes por sua vez disponibilizam um objeto pelo qual os clientes executarão funções nas máquinas controladoras.

Os clientes ficam localizados em **MQTT-RMI-CLIENT** e **MQTT-RMI-CLIENT-2**.

### Controladores 

Os controladores são sistemas que farão o meio de campo entre os clientes e o sensor, atuador e banco de dados. Ao serem iniciados eles sobem o seu respectivo **servidor RMI**, para que os clientes possam se conectar.

Os controladores ficam localizados em **MQTT-RMI-CONTROLLER** **MQTT-RMI-CONTROLLER-2**.

### Sensor 

O sensor gera temperaturas aleatórias dentro de um intervalo, e manda as temperaturas de 5 em 5 segundos para o **Servidor MQTT**. 

O sensor fica localizado em **MQTT-RMI-SENSOR**.

### Atuador

O atuador é reponsável por mudar o intervalo de geração de temperaturas aleatórias, ou seja, ele aumenta e/ou diminui as temperaturas geradas pelo sensor. 

O atuado fica localizado em **MQTT-RMI-TEMP-MANAGER**.

### Banco de dados

Cada controlador acessa um banco diferente, no banco são salvas as informações de temperatura assim como a data / hora em que a temperatura foi 
capturada.












