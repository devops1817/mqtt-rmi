package igor.rocha.MQTT;

import igor.rocha.RandomTemp;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TempPublisher {
        private static final String BROKER_URL = "tcp://localhost:1883";
        private static final String CLIENT_ID = "sensor_temp_publisher";
        private static final String TOPIC = "greenhouse_temp";

        private static final RandomTemp random = new RandomTemp();

        private static Double minimumTemp;

        private static Double maximumTemp;

        private MqttClient client;
        private ScheduledExecutorService executor;

        private final String PATTERN_DATE = "dd/MM/yyyy:HH:mm:ss";

        public TempPublisher (double minimumTemp, double maximumTemp) throws MqttException {
            client = new MqttClient(BROKER_URL, CLIENT_ID, new MemoryPersistence());
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            client.connect(options);
            TempPublisher.maximumTemp = maximumTemp;
            TempPublisher.minimumTemp = minimumTemp;
            executor = Executors.newSingleThreadScheduledExecutor();
        }


        public void start() {
            executor.scheduleAtFixedRate(() -> {
                double randomNumber = random.getRandomTemp(TempPublisher.minimumTemp, TempPublisher.maximumTemp);
                String strMessage = LocalDateTime.now().format(DateTimeFormatter.ofPattern(PATTERN_DATE)) + " " + String.valueOf(randomNumber);
                MqttMessage message = new MqttMessage(strMessage.getBytes());
                try {
                    client.publish(TOPIC, message);
                    System.out.println("Mensagem enviada: " + strMessage);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }, 0, 5, TimeUnit.SECONDS);
        }


        public void stop() {
            try {
                executor.shutdown();
                client.disconnect();
                client.close();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }


    public static Double getMinimumTemp() {
        return minimumTemp;
    }
 
    public static Double getMaximumTemp() {
        return maximumTemp;
    }

    public static void setMinimumTemp(Double minimumTemp) {

            TempPublisher.minimumTemp += minimumTemp;
    }

    public static void setMaximumTemp(Double maximumTemp) {

            TempPublisher.maximumTemp += maximumTemp;
    }
}
