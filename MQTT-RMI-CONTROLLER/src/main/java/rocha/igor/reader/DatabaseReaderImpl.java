package rocha.igor.reader;

import rocha.igor.reader.interfaces.DatabaseReader;
import rocha.igor.util.Utils;

public class DatabaseReaderImpl implements DatabaseReader {

    static final String DB_CONFIG_PATH = "src/main/resources/db.properties";

    @Override
    public String getUser() {
        return Utils.loadProperties("user", DB_CONFIG_PATH);
    }

    @Override
    public String getPassword() {
        return Utils.loadProperties("password", DB_CONFIG_PATH);
    }

    @Override
    public String getUrl() {
        return Utils.loadProperties("dburl", DB_CONFIG_PATH);
    }
}
