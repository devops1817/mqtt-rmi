package rocha.igor.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utils {

    public static String loadProperties(final String propertyName, final String archivePath) {

        Properties properties = new Properties();
        try (FileInputStream inputStream = new FileInputStream(archivePath)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(propertyName);
    }

}
