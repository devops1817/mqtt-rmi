package rocha.igor.reader.interfaces;

public interface MqttServerReader {
    String getBrokerUrl();

}
