package rocha.igor;

import org.eclipse.paho.client.mqttv3.MqttException;
import rocha.igor.controller.interfaces.GreenhouseTempController;
import rocha.igor.entities.GreenhouseTemp;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        try{
            Registry registry = LocateRegistry.getRegistry("localhost", 1099);
            GreenhouseTempController controller = (GreenhouseTempController) registry.lookup("greenhouse_service");
            executeMenu(controller);
        }catch (ConnectException e){
            try{
                Registry registry = LocateRegistry.getRegistry("localhost", 1098);
                GreenhouseTempController controller = (GreenhouseTempController) registry.lookup("greenhouse_service");
                executeMenu(controller);

            }catch (Exception f){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private static void executeMenu(GreenhouseTempController controller) throws RemoteException, InterruptedException, MqttException {
        int choice;
        do{

            printMenu();
            choice = chooseOption(controller);

        }while(choice != 6);

    }


    private static void printMenu(){
        System.out.print("-------------------------------------------------\n" +
                "1 - Acompanhar a temperatura em tempo real.\n" +
                "2 - Diminuir a temperatura da estufa.\n" +
                "3 - Aumentar a temperatura da estufa.\n" +
                "4 - Mudar temperatura máxima ou/e mínima.\n" +
                "5 - Consultar temperaturas anteriores.\n" +
                "-------------------------------------------------\n" +
                "Escolha: ");
    }
    private static int chooseOption(GreenhouseTempController controller) throws RemoteException, InterruptedException, MqttException {
        int choice;
        Scanner in = new Scanner(System.in);
        choice = in.nextInt();

        switch (choice){
            case 1:
                realTimeTemp(controller);
                break;
            case 2:
                upperOrLessTemp(controller, false);
                break;
            case 3:
                upperOrLessTemp(controller, true);
                break;
            case 5:
                showTemps(controller);
                break;

        }

        return choice;

    }

    private static void upperOrLessTemp(GreenhouseTempController controller, boolean upperOrLess) throws MqttException, RemoteException {
        String question = "Escolha o quanto quer ";
        question += ( upperOrLess ) ? "aumentar: " : "diminuir";
        System.out.print(question);
        Scanner in = new Scanner(System.in);
        double temp = in.nextDouble();
        if (upperOrLess) controller.increaseTemp(temp);
        else controller.lowerTemp(temp);

    }

    private static void showTemps(GreenhouseTempController controller) throws RemoteException {
        Scanner in =  new Scanner(System.in);
        String date = in.nextLine();
        List<GreenhouseTemp> listaTemp = controller.searchByDate(Date.valueOf(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        printTemps(listaTemp);

    }

    private static void printTemps(List<GreenhouseTemp> listaTemp ){
        System.out.println("|-------DIA--------|---------HORA--------|---------TEMPERATURA--------|");
        if(listaTemp != null){
            listaTemp.forEach(
                    (temp) -> {
                        System.out.println("|----" + temp.getDateTemp().toString() + "----|" +
                                "|----" + temp.getTimeTemp() + "----|" + "|----" + temp.getTemp() + "----|");
                    }
            );
        }else{
            System.out.println("Nenhuma temperatura registrada nessa data");
        }

    }


    private static void realTimeTemp(GreenhouseTempController controller) throws RemoteException, InterruptedException {
        long startTime = System.currentTimeMillis();

        long timeLimit = 35000;

        while (System.currentTimeMillis() - startTime < timeLimit) {
            System.out.println(controller.getDateLastTemp() + " " +controller.getLastTemp());
            Thread.sleep(5000);
        }

    }
}