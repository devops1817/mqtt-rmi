package rocha.igor.reader;

import rocha.igor.reader.interfaces.MqttServerReader;
import rocha.igor.util.Utils;

public class MqttServerReaderImpl implements MqttServerReader {

    static final String MQTT_CONFIG_PATH = "src/main/resources/mqtt_server.properties";

    @Override
    public String getBrokerUrl() {
        return Utils.loadProperties("broker.url", MQTT_CONFIG_PATH);
    }
 
}
