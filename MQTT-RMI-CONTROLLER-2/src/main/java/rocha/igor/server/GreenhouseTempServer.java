package rocha.igor.server;

import rocha.igor.controller.GreenhouseControllerImpl;
import rocha.igor.reader.ServerReaderImpl;
import rocha.igor.reader.interfaces.ServerReader;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class GreenhouseTempServer {

    public GreenhouseTempServer(){
        try{
            ServerReader reader =  new ServerReaderImpl();
            Registry registry = LocateRegistry.createRegistry(reader.getPort());
            registry.bind(reader.getService(), new GreenhouseControllerImpl());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
