package rocha.igor.dao;

import rocha.igor.reader.DatabaseReaderImpl;
import rocha.igor.reader.interfaces.DatabaseReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ManageConnection {

    private static Connection conn = null;

    // Create and return connection with database
    public static Connection CreateDatabaseConnection() throws SQLException {
        if (conn == null || conn.isClosed() == true) {
            try {
                DatabaseReader dbReader = new DatabaseReaderImpl();
                String user = dbReader.getUser();
                String password = dbReader.getPassword();
                String dburl = dbReader.getUrl();
                conn = DriverManager.getConnection(dburl, user, password);

            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Connection database error");
                return null;
            }

        }
        return conn;


    }

}
