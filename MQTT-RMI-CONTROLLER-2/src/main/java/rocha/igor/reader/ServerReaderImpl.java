package rocha.igor.reader;

import rocha.igor.reader.interfaces.ServerReader;
import rocha.igor.util.Utils;

public class ServerReaderImpl implements ServerReader {

    static final String SERVER_CONFIG_PATH = "src/main/resources/server.properties";

    @Override
    public Integer getPort() {
        return Integer.valueOf(Utils.loadProperties("server.port", SERVER_CONFIG_PATH));
    }

    @Override
    public String getService() {
       return Utils.loadProperties("server.service", SERVER_CONFIG_PATH);
    }

}
