package igor.rocha.MQTT;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class TempPublisher {

    private static final String BROKER_URL = "tcp://localhost:1883";
    private static final String CLIENT_ID = "greenhouse_manager_pub";
    private static final String TOPIC = "greenhouse_temp_manager_to_sensor";

    private MqttClient client;


    public TempPublisher() throws MqttException {
        client = new MqttClient(BROKER_URL, CLIENT_ID, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        client.connect(options);
    }


    public void publishTemp(MqttMessage message) throws MqttException {
        client.publish(TOPIC, message);
    }
}
