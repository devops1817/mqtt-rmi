package rocha.igor.service;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import rocha.igor.controller.GreenhouseControllerImpl;
import rocha.igor.reader.MqttServerReaderImpl;
import rocha.igor.reader.interfaces.MqttServerReader;

public class TempSubscribeService implements MqttCallback {

    private static final String CLIENT_ID = "controller_2_temp_listener";
    private static final String TOPIC = "greenhouse_temp";

    private static MqttServerReader reader = new MqttServerReaderImpl();

    private MqttClient client;

    public TempSubscribeService() throws MqttException {
        client = new MqttClient(reader.getBrokerUrl(), CLIENT_ID, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        client.connect(options);
        client.setCallback(this);
        client.subscribe(TOPIC);
    }

    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("Conexão perdida");
        System.out.println(cause.toString());
    }


    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        System.out.println("Mensagem recebida " + message);
        String strMessage = message.toString();
        String time = strMessage.substring(0, 20);
        String temp = strMessage.substring(20);
        GreenhouseControllerImpl.setTemp(time.trim(), temp.trim());

    }


    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }
}
