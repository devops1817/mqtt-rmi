package rocha.igor.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class GreenhouseTemp implements Serializable {
 
    private Long id;
    private LocalDate dateTemp;
    private String timeTemp;
    private Double temp;
    private State state;

    public GreenhouseTemp(){

    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateTemp() {
        return dateTemp;
    }

    public void setDateTemp(LocalDate dateTemp) {
        this.dateTemp = dateTemp;
    }

    public String getTimeTemp() {
        return timeTemp;
    }

    public void setTimeTemp(String timeTemp) {
        this.timeTemp = timeTemp;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof rocha.igor.entities.GreenhouseTemp that)) return false;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "GreenhouseTemp{" +
                "id=" + id +
                ", dateTemp=" + dateTemp +
                ", timeTemp='" + timeTemp + '\'' +
                ", temp=" + temp +
                ", state=" + state +
                '}';
    }
}


