package rocha.igor.entities;

public enum State {

    LESS("menor"),
    NORMAL("normal"),
    GREATER("maior");

    private String name;


    private State(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "State{" +
                "name='" + name + '\'' +
                '}';
    }
}
