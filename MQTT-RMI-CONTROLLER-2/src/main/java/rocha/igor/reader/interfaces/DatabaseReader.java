package rocha.igor.reader.interfaces;

public interface DatabaseReader {

    String getUser();
    String getPassword();
    String getUrl();

}
