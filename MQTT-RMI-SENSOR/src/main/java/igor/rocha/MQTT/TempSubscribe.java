package igor.rocha.MQTT;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.sql.SQLOutput;

public class TempSubscribe {

    private static final String BROKER_URL = "tcp://localhost:1883";
    private static final String CLIENT_ID = "sensor_temp_sub";
    private static final String TOPIC = "greenhouse_temp_manager_to_sensor";

    private static final String PATTERN_MESSAGE = ":\\s(\\d+\\.\\d+)";

    private static final String PATTERN_TEMP = ".+:\\s";




    private MqttClient client;


    public TempSubscribe() throws MqttException {
        client = new MqttClient(BROKER_URL, CLIENT_ID, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        client.connect(options);
        client.subscribe(TOPIC, new IMqttMessageListener() {
            @Override
            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                String moreOrLess = mqttMessage.toString().replaceAll(PATTERN_MESSAGE,"");
                double temp;
                if(moreOrLess.equals("less_temp")) temp = (Double.parseDouble(mqttMessage.toString().replaceAll(PATTERN_TEMP,"")) * - 1);
                else temp = Double.parseDouble(mqttMessage.toString().replaceAll(PATTERN_TEMP,""));

                TempPublisher.setMaximumTemp(temp);
                TempPublisher.setMinimumTemp(temp);


            }
        });
    }

}
