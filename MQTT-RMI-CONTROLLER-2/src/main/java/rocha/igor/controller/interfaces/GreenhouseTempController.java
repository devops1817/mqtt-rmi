package rocha.igor.controller.interfaces;

import org.eclipse.paho.client.mqttv3.MqttException;
import rocha.igor.entities.GreenhouseTemp;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Date;
import java.util.List;

/**
 * Esta interface descreve todos os métodos que poderão ser usados pelo cliente
 * @autor Igor Guilherme Almeida Rocha
 */
public interface GreenhouseTempController extends Remote {

    String getLastTemp() throws RemoteException;
    String getDateLastTemp() throws RemoteException;


    void setMaxTemp(double maxTemp) throws RemoteException;
    void setMinTemp(double minTemp) throws  RemoteException;

    void lowerTemp(double temp)  throws  RemoteException, MqttException;
    void increaseTemp(double temp) throws RemoteException, MqttException;

    List<GreenhouseTemp> searchByDate(Date date) throws RemoteException;

}
